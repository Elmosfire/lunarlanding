from re import I
from bidict import bidict
from itertools import product
from tqdm import tqdm
from collections import namedtuple
from lxml.html import builder as H
from lxml import html

from sqlalchemy import create_engine, MetaData, Table, func as f
from sqlalchemy import Column, ForeignKey, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.expression import literal, exists
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import relationship, aliased
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
engine = create_engine("sqlite:///puzzles.db", echo=True)


field_names = bidict()


for y in range(5):
    for x in range(5):
        field_names[(x, y)] = "abcdefghijklmnopqrstuvwxy"[y * 5 + x]


class InvalidState(AssertionError):
    pass


class State(Base):
    __tablename__ = "state"
    # Here we define columns for the table person
    # Notice that each column is also a normal Python instance attribute.
    id = Column(String(6), primary_key=True)
    isfinal = Column(Boolean)

    def verify_if_valid(self):
        if not all(self.id[x] < self.id[x + 1] for x in range(1, len(self.id) - 1)):
            raise InvalidState(self.id)

    def store(self, session):
        self.verify_if_valid()
        session.add(self)
        # print("stored", self.id)

    @classmethod
    def generate_all(cls, DBSession):
        for x in tqdm(range(25)):
            with DBSession.begin() as session:
                for a in range(20):
                    for b in range(a + 1, 21):
                        for c in range(b + 1, 22):
                            for d in range(c + 1, 23):
                                for e in range(d + 1, 24):
                                    if x in [a, b, c, d, e]:
                                        continue
                                    try:
                                        cls(
                                            id="".join(
                                                map(
                                                    lambda i: "abcdefghijklmnopqrstuvwxy"[
                                                        i
                                                    ],
                                                    [x, a, b, c, d, e],
                                                )
                                            ),
                                            isfinal=(x == 12),
                                        ).store(session)
                                    except InvalidState:
                                        raise
                                if x in [a, b, c, d]:
                                    continue
                                try:
                                    cls(
                                        id="".join(
                                            map(
                                                lambda i: "abcdefghijklmnopqrstuvwxy"[
                                                    i
                                                ],
                                                [x, a, b, c, d],
                                            )
                                        ),
                                        isfinal=(x == 12),
                                    ).store(session)
                                except InvalidState:
                                    raise
                            if x in [a, b, c]:
                                continue
                            try:
                                cls(
                                    id="".join(
                                        map(
                                            lambda i: "abcdefghijklmnopqrstuvwxy"[i],
                                            [x, a, b, c],
                                        )
                                    ),
                                    isfinal=(x == 12),
                                ).store(session)
                            except InvalidState:
                                raise


class Moves(Base):
    __tablename__ = "moves"
    from_ = Column(String(6), ForeignKey(State.id), primary_key=True)
    to_ = Column(String(6), ForeignKey(State.id), primary_key=True)
    start = Column(String(1))
    stop = Column(String(1))
    direction = Column(String(1))

    startstate = relationship("State", foreign_keys=[from_])
    endstate = relationship("State", foreign_keys=[to_])


class OptMoves(Base):
    __tablename__ = "optmoves"
    from_ = Column(String(6), ForeignKey(Moves.from_), primary_key=True)
    to_ = Column(String(6), ForeignKey(Moves.to_), primary_key=True)

    # move = relationship("Moves", foreign_keys=[from_, to_])


class Bassin(Base):
    __tablename__ = "bassin"
    stateid = Column(String(6), ForeignKey(State.id), primary_key=True)
    value = Column(Integer)
    state = relationship("State", foreign_keys=[stateid])


def get_rows():
    for start in range(3):
        for stop in range(start + 2, 5):
            for y in range(5):
                yield [(x, y) for x in range(start, stop + 1)]


ValidMove = namedtuple("ValidMove", "start, stop, block, exclude, direction")


def valid_moves():
    for s in get_rows():
        mirror = [(y, x) for x, y in s]
        rev = list(reversed(s))
        revmirror = list(reversed(mirror))
        for direction, line in (("R", s), ("D", mirror), ("L", rev), ("U", revmirror)):
            yield ValidMove(
                start=field_names[line[0]],
                stop=field_names[line[-2]],
                exclude="".join(map(field_names.get, line[1:-1])),
                block=field_names[line[-1]],
                direction=direction,
            )


VALID_MOVES = list(valid_moves())


def state_with_field_replaced(stateid, old, new):
    rep = stateid.replace(old, new)
    return rep[0] + "".join(sorted(rep[1:]))


def get_possible_moves(stateid):
    tiles = set(stateid)
    for x in VALID_MOVES:
        if set(x.exclude) & tiles:
            continue
        elif not x.block in tiles:
            continue
        elif not x.start in tiles:
            continue
        else:
            yield Moves(
                from_=stateid,
                to_=state_with_field_replaced(stateid, x.start, x.stop),
                start=x.start,
                stop=x.stop,
                direction=x.direction,
            )


def generate_possible_moves(DBSession):
    with DBSession.begin() as session:
        states = list(map(lambda x: x.id, session.query(State)))
    print(states[:10])
    with DBSession.begin() as session:
        for i, stateid in enumerate(tqdm(states, desc="calculating_moves")):
            for move in get_possible_moves(stateid):
                session.add(move)


Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
Base.metadata.create_all()


def regen_database(DBSession):
    State.generate_all(DBSession)
    generate_possible_moves(DBSession)
    with DBSession.begin() as session:
        build_bassin(session)
        store_optimal_moves(session)


def final_states(session):
    return session.query(State).filter(State.isfinal == True)


def get_endpoints_from_bassin(session):
    return (
        session.query(Moves)
        .filter(~exists().where(Moves.from_ == Bassin.stateid))
        .filter(exists().where(Moves.to_ == Bassin.stateid))
        .distinct()
    )


# regen_database(DBSession)
def build_bassin(session):
    Bassin.__table__.delete().execute()
    Base.metadata.create_all()
    # for x in final_states(session).with_entities(State.id, literal(0)):
    #    print(x)
    Bassin.__table__.insert().from_select(
        names=["stateid", "value"],
        select=final_states(session).with_entities(
            State.id.label("stateid"), literal(0).label("value")
        ),
    ).execute()
    for i in range(1, 20):
        Bassin.__table__.insert().from_select(
            names=["stateid", "value"],
            select=get_endpoints_from_bassin(session).with_entities(
                Moves.from_.label("stateid"), literal(i).label("value")
            ),
        ).execute()


def store_optimal_moves(session):
    BassinLeft = aliased(Bassin)
    BassinRight = aliased(Bassin)
    q = (
        session.query(Moves)
        .join(BassinLeft, Moves.from_ == BassinLeft.stateid)
        .join(BassinRight, Moves.to_ == BassinRight.stateid)
        .where(BassinLeft.value == BassinRight.value + 1)
        .with_entities(Moves.from_, Moves.to_)
    )
    for x in q.limit(10):
        print(x)
    OptMoves.__table__.insert().from_select(
        names=["from_", "to_"],
        select=q,
    ).execute()


def analyse(DBSession):
    with DBSession.begin() as session:
        tot = sum(1 for _ in session.query(State))
        val = sum(1 for _ in session.query(Bassin))
        print(f"valid {val}/{tot}")
        for x in (
            session.query(Bassin)
            .group_by(Bassin.value)
            .with_entities(f.count(Bassin.stateid).label("freq"), Bassin.value)
        ):
            print(f"puzzles solved in {x.value} moves: {x.freq}")


def get_html_widget(state, name):
    return html.tostring(
        H.TABLE(
            H.TR(H.TD(name, colspan="5")),
            H.TR(
                H.TD(str(state.size()), colspan="3"),
                H.TD(str(state.difficulty()), colspan="2"),
            ),
            *[
                H.TR(
                    *[
                        H.TD(
                            " ",
                            bgcolor=(
                                "red"
                                if state[0] == field_names[x, y]
                                else "black"
                                if field_names[x, y] in state
                                else "white"
                            ),
                            width="20%",
                        )
                        for x in range(5)
                    ]
                )
                for y in range(5)
            ],
        )
    ).decode("utf-8")
